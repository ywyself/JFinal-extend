## JFinal-extend
#### 项目介绍
在jfinal框架上扩展一些功能

#### 注解
- 使用注解进行参数验证

#### 指令集扩展
- 转义HTML标签
- 转义JavaScript字符
- URL编码
- 时间版本号

#### 图片验证码
> 基于[EasyCaptcha](!https://gitee.com/whvse/EasyCaptcha)
- 普通图片验证码![png](./web/images/c1.jpg "")
- 中文验证码![png](./web/images/c3.jpg "")
- GIF图验证码![png](./web/images/c.jpg "")![png](./web/images/c2.jpg "")

#### 二维码
- 支持logo设置
- 支持背景设置

![logo](./web/images/qrCode.png "添加为项目的lib")

#### Excel
- Excel文档创建下载



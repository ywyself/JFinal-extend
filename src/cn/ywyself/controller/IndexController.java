package cn.ywyself.controller;

import cn.ywyself.extend.aop.validate.DataType;
import cn.ywyself.extend.aop.validate.PreInterceptor;
import cn.ywyself.extend.aop.validate.PreValidator;
import cn.ywyself.extend.aop.validate.PreValidators;
import cn.ywyself.extend.render.YsCaptchaRender;
import cn.ywyself.extend.render.YsExcelRender;
import cn.ywyself.extend.render.YsQrCodeRender;
import cn.ywyself.main.BaseController;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;

import java.util.ArrayList;
import java.util.List;

/**
 * 控制器
 *
 * @author : ywyself
 * @created : 2018-10-07 14:05
 */
public class IndexController extends BaseController {

    /**
     * 测试首页，用来测试扩展指令集
     */
    public void index() {
        render("/index.html");
    }

    /**
     * 测试Excel文件导出
     */
    public void excel() {
        List<List<Object>> data = new ArrayList<>(10);
        List<Object> head = new ArrayList<>(10);
        head.add("行");
        data.add(head);
        for (int i = 0; i < 10; i++) {
            List<Object> row = new ArrayList<>(10);
            for (int j = 0; j < 10; j++) {
                row.add(10 * i + j);
            }
            data.add(row);
        }
        render(new YsExcelRender(data));
    }

    /**
     * 二维码渲染
     */
    public void qr() {
        String text = getRequest().getQueryString();
        if (StrKit.isBlank(text)) {
            render(new YsQrCodeRender("测试文本", "/qr/logo.png", "/qr/bg.png"));
        } else {
            render(new YsQrCodeRender(text));
        }
    }

    /**
     * 渲染一个图片验证码
     */
    public void captcha() {
        render(new YsCaptchaRender(getPara("s")));
    }

    /**
     * 验证码
     */
    public void ver() {
        String captcha = getPara("code", "");
        if (StrKit.isBlank(captcha)) {
            response("10001", "验证码不能为空");
        }
        if (YsCaptchaRender.verify(this, captcha, "test")) {
            response("10000", "验证成功");
        } else {
            response("10001", "验证失败");
        }
    }

    /**
     * 使用注解进行参数验证
     */
    @Before(PreInterceptor.class)
    @PreValidators({
            @PreValidator(field = "i", desc = "int类型", dataType = DataType.INTEGER, min = 2, max = 255),
            @PreValidator(field = "d", desc = "double类型", dataType = DataType.DOUBLE, min = 0.2),
            @PreValidator(field = "email", desc = "email", dataType = DataType.EMAIL),
            @PreValidator(field = "tel", desc = "tel", dataType = DataType.TEL),
            @PreValidator(field = "qq", desc = "qq", dataType = DataType.QQ),
            @PreValidator(field = "cardNo", desc = "银行卡号", dataType = DataType.BANK_NO),
            @PreValidator(field = "idCard", desc = "idCard", dataType = DataType.ID_CARD)
    })
    @PreValidator(field = "userName", desc = "用户名", dataType = DataType.STRING)
    public void vd() {
        int id;
        try {
            id = getPkId("id");
        } catch (Exception e) {
            response("10001", e.getMessage());
            return;
        }
        renderText("OK");
    }
}

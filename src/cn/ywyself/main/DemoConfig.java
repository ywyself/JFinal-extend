package cn.ywyself.main;

import cn.ywyself.extend.directive.*;
import cn.ywyself.extend.handle.WebResourceHandler;
import com.jfinal.config.*;
import com.jfinal.core.ActionReporter;
import com.jfinal.core.JFinal;
import com.jfinal.template.Engine;

/**
 * @author ywyself
 */
public class DemoConfig extends JFinalConfig {

    /**
     * 运行此 main 方法可以启动项目
     */
    public static void main(String[] args) {
        JFinal.start("web", 80, "/");
    }

    /**
     * 加载配置文件
     */
    @Override
    public void afterJFinalStart() {
        // 设置先打印请求报告
        ActionReporter.setReportAfterInvocation(false);
    }

    /**
     * 配置参数
     */
    @Override
    public void configConstant(Constants me) {
        me.setDevMode(true);
    }

    /**
     * 配置模板引擎
     */
    @Override
    public void configEngine(Engine me) {
        me.addDirective("esc", EscapeUtilsDirective.class);
        me.addDirective("escHtml", EscapeHtmlDirective.class);
        me.addDirective("escJs", EscapeJavaScriptDirective.class);
        me.addDirective("ver", VersionDirective.class);
        me.addDirective("url", UrlEncodeDirective.class);
    }

    /**
     * 管理所有的web请求
     */
    @Override
    public void configHandler(Handlers me) {
        me.add(new WebResourceHandler());
    }

    /**
     * 全局拦截器
     */
    @Override
    public void configInterceptor(Interceptors me) {
    }

    /**
     * 配置插件
     */
    @Override
    public void configPlugin(Plugins me) {
    }

    /**
     * 配置路由
     */
    @Override
    public void configRoute(Routes me) {
        me.add("/", cn.ywyself.controller.IndexController.class);
    }
}

package cn.ywyself.main;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 注释
 *
 * @Author: ywyself
 * @Created: 2018-10-07 14:04
 */
public class BaseController extends Controller {

    /**
     * 读取页码
     *
     * @param name 页码参数名
     * @return 页码
     */
    public int getPageNumber(String name) {
        int iPage = 1;
        try {
            iPage = Integer.parseInt(getPara(name, "1"));
            if (iPage < 1) {
                iPage = 1;
            }
        } catch (NumberFormatException ignored) {
        }
        return iPage;
    }

    /**
     * 获取日期格式参数的值
     *
     * @param name       参数名
     * @param defaultVal 默认值
     * @return 日期格式参数，转换出错时返回默认值
     */
    public String getDateStr(String name, String defaultVal) {
        String dateStr = getPara(name, defaultVal);
        String ret;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            Date d1 = sdf.parse(dateStr);
            c.setTime(d1);
            ret = c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DAY_OF_MONTH);
            // 时间 c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
        } catch (Exception e) {
            return defaultVal;
        }
        return ret;
    }

    /**
     * 获取主键id
     *
     * @param name 参数名
     * @return id
     * @throws Exception id错误提示
     */
    public int getPkId(String name) throws Exception {
        String sId = getPara(name);
        int id;
        if (StrKit.isBlank(sId)) {
            throw new Exception("ID参数不能为空");
        }
        try {
            id = Integer.parseInt(sId);
            if (id < 1) {
                throw new Exception("ID错误");
            }
        } catch (NumberFormatException ignored) {
            throw new Exception("ID只能为整数");
        }
        return id;
    }

    public List<Integer> getPkIdList(String name) throws Exception {
        String ids = getPara(name, "");
        if (StrKit.isBlank(ids)) {
            throw new Exception("ID参数不能为空");
        }
        List<Integer> idList = new ArrayList<>();
        String[] idArr = ids.split(",");
        try {
            int i;
            for (String s : idArr) {
                i = Integer.parseInt(s);
                if (i < 1) {
                    throw new Exception("ID错误");
                }
                idList.add(i);
            }
        } catch (NumberFormatException e) {
            throw new Exception("ID只能为整数");
        }
        return idList;
    }

    /**
     * 响应json数据
     *
     * @param code   状态码
     * @param errMsg 提示信息
     */
    public void response(String code, String errMsg) {
        response(code, errMsg, null);
    }

    /**
     * 响应json数据
     *
     * @param code   状态码
     * @param errMsg 提示信息
     * @param data   数据
     */
    public void response(String code, String errMsg, Map data) {
        int size = data != null ? data.size() + 2 : 2;
        Map<String, Object> response = new HashMap<>(size);
        response.put("code", code);
        response.put("errMsg", errMsg);
        if (data != null) {
            response.put("data", data);
        }
        renderJson(response);
    }
}

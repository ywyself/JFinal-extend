package cn.ywyself.extend.aop.validate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据验证，对单个，单个验证优先于多验证
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PreValidator {

    /**
     * 属性名。如：userName
     */
    String field();

    /**
     * 默认值
     */
    String value() default "";

    /**
     * 属性说明，如：用户名
     */
    String desc();

    /**
     * 数据类型/验证数据类型
     */
    DataType dataType() default DataType.STRING;

    /**
     * 数据类型为int/double时：值>=min <br>
     * 数据类型为String时：字符数>=min <br>
     */
    double min() default PreConst.DISMISS;

    /**
     * 数据类型为int/double时：值<=max <br>
     * 数据类型为String时：字符数<=max <br>
     */
    double max() default PreConst.DISMISS;
}

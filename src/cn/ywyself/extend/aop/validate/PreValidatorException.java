package cn.ywyself.extend.aop.validate;

/**
 * 异常
 *
 * @Author: ywyself
 * @Created: 2018-10-09 11:44
 */
public class PreValidatorException extends RuntimeException {
    private static final long serialVersionUID = 20920496215941871L;
}

package cn.ywyself.extend.aop.validate;

/**
 * 验证数据类型
 *
 * @Author: ywyself
 * @Created: 2018-10-09 10:19
 */
public enum DataType {
    /**
     * 文本不能为空
     */
    STRING,
    /**
     * 必须为int类型
     */
    INTEGER,
    /**
     * 必须为double类型
     */
    DOUBLE,
    /**
     * url地址
     */
    URL,
    /**
     * 邮箱地址
     */
    EMAIL,
    /**
     * 手机号
     */
    TEL,
    /**
     * QQ号
     */
    QQ,
    /**
     * 中文姓名
     */
    NAME,
    /**
     * 银行卡号
     */
    BANK_NO,
    /**
     * 身份证号
     */
    ID_CARD

}

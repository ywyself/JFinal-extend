package cn.ywyself.extend.utils;

import com.jfinal.core.Controller;
import com.jfinal.render.RenderManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 视图渲染
 */
@SuppressWarnings({"serial", "rawtypes", "unchecked"})
public class RenderUtils {

    /**
     * 获取渲染后视图的html
     *
     * @param viewPath 视图路径
     * @param data     页面注入值，可以为null
     */
    public static String getViewHtml(String viewPath, Map<String, Object> data) {
        return RenderManager.me().getEngine().getTemplate(viewPath).renderToString(data);
    }

    /**
     * 返回alert的js代码
     *
     * @param controller 控制器
     * @param errMsg     错误提示
     * @param targetUrl  目标地址
     */
    public static void renderAlert(Controller controller, String errMsg, String targetUrl) {
        RenderUtils.renderAlert(controller.getRequest(), controller.getResponse(), null, errMsg, targetUrl);
    }

    /**
     * 返回alert的js代码
     *
     * @param errMsg    错误提示
     * @param targetUrl 目标地址
     */
    public static void renderAlert(HttpServletRequest request, HttpServletResponse response, boolean[] isHandled,
                                   String errMsg, String targetUrl) {
        if (isHandled != null) {
            isHandled[0] = true;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<script>");
        sb.append("alert('").append(EscapeUtils.escapeJavaScript(errMsg)).append("');");
        if (targetUrl != null) {
            sb.append("window.location.replace('").append(EscapeUtils.escapeJavaScript(targetUrl)).append("');");
        } else {
            sb.append("window.history.go(-1)");
        }
        sb.append("</script>");
        RenderManager.me().getRenderFactory().getHtmlRender(sb.toString()).setContext(request, response).render();
    }

    /**
     * 渲染一个视图，在handler链中调用
     *
     * @param request   请求
     * @param response  响应
     * @param isHandled [引用传入]
     * @param view      视图路径
     * @param data      页面注入值，可以为null
     */
    public static void renderView(HttpServletRequest request, HttpServletResponse response, boolean[] isHandled,
                                  String view, Map<String, Object> data) {
        isHandled[0] = true;
        if (data != null) {
            for (Entry<String, Object> e : data.entrySet()) {
                request.setAttribute(e.getKey(), e.getValue());
            }
        }
        RenderManager.me().getRenderFactory().getRender(view).setContext(request, response).render();
    }

    /**
     * 返回json格式收据，在handler链中调用
     *
     * @param request   请求
     * @param response  响应
     * @param isHandled 在调用了 ActionHandler的 handle 方法之后，isHandled[0] 才会被置为 true。
     * @param data      map
     */
    public static void renderJson(HttpServletRequest request, HttpServletResponse response,
                                  boolean[] isHandled, Map<String, Object> data) {
        isHandled[0] = true;
        RenderManager.me().getRenderFactory().getJsonRender(data).setContext(request, response).render();
    }

    /**
     * 返回错误码，在handler链中调用
     *
     * @param request   请求
     * @param response  响应
     * @param isHandled 在调用了 ActionHandler的 handle 方法之后，isHandled[0] 才会被置为 true。
     * @param errorCode 错误码
     */
    public static void renderError(HttpServletRequest request, HttpServletResponse response, boolean[] isHandled, int errorCode) {
        isHandled[0] = true;
        RenderManager.me().getRenderFactory().getErrorRender(errorCode).setContext(request, response).render();
    }

}

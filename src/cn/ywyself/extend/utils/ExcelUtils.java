package cn.ywyself.extend.utils;

import com.google.zxing.WriterException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

/**
 * excel文档生成
 *
 * @author : ywyself
 * @created : 2018-10-12 17:20
 */
public class ExcelUtils {
    /**
     * excel 文档
     */
    private HSSFWorkbook _workbook = null;

    /**
     * 工作簿
     */
    private HSSFSheet _sheet = null;

    /**
     * 单元行
     */
    private HSSFRow _row = null;

    /**
     * 列索引
     */
    private int colIndex = 0;
    /**
     * 行索引
     */
    private int rowIndex = 0;

    private ExcelUtils() {
    }

    /**
     * 初始化
     *
     * @return 返回当前对象
     */
    public static ExcelUtils init() {
        ExcelUtils excelUtils = new ExcelUtils();
        // 创建文档
        excelUtils._workbook = new HSSFWorkbook();
        return excelUtils;
    }

    /**
     * 检查工作簿是否存在，不存在则创建一个
     */
    private void checkSheet() {
        if (this._sheet == null) {
            this.createSheet(null);
        }
    }

    private void checkRow() {
        if (this._row == null) {
            this.createRow();
        }
    }

    /**
     * 重置行索引
     */
    private void resetRowIndex() {
        this.rowIndex = 0;
    }

    /**
     * 重置列索引
     */
    private void resetColIndex() {
        this.colIndex = 0;
    }


    /**
     * 创建工作簿
     */
    public void createSheet() {
        createSheet(null);
    }

    /**
     * 创建工作簿
     *
     * @param sheetName 工作簿名称
     */
    public void createSheet(String sheetName) {
        if (sheetName == null || "".equals(sheetName.trim())) {
            _sheet = _workbook.createSheet();
        } else {
            _sheet = _workbook.createSheet(sheetName);
        }
        // 初始化行索引
        resetRowIndex();
    }

    /**
     * 创建行
     */
    public void createRow() {
        checkSheet();
        // 设置单元格默认宽度
        _sheet.setDefaultColumnWidth((short) 20);
        // 创建一个行对象
        _row = _sheet.createRow(rowIndex++);
        // 设置行高20像素
        _row.setHeightInPoints(15);
        // 初始化列索引
        resetColIndex();
    }

    public void addCell() {
        checkRow();
        // 单元格索引增加
        colIndex++;
    }

    public void addCell(String value) {
        checkRow();
        // 创建单元格
        HSSFCell cell = _row.createCell(colIndex++);
        // 设置内容
        cell.setCellValue(value);
        setCellDataFormat(cell, "@");
    }

    public void addCell(double value) {
        addCell(value, null);
    }

    public void addCell(double value, String format) {
        checkRow();
        HSSFCell cell = this._row.createCell(colIndex++);
        cell.setCellValue(value);
        setCellDataFormat(cell, format);
    }

    public void addCell(boolean value) {
        checkRow();
        HSSFCell cell = this._row.createCell(colIndex++);
        cell.setCellValue(value);
    }

    public void addCell(Date value) {
        addCell(value, "yyyy/MM/dd HH:mm:ss");
    }

    public void addCell(Date value, String format) {
        checkRow();
        HSSFCell cell = this._row.createCell(colIndex++);
        cell.setCellValue(value);
        setCellDataFormat(cell, format);
    }

    /**
     * 设置单元格数据格式，为null时不设置格式
     *
     * @param cell   单元格
     * @param format 格式
     */
    private void setCellDataFormat(HSSFCell cell, String format) {
        if (format != null) {
            // 创建样式对象
            HSSFCellStyle cellStyle = _workbook.createCellStyle();
            // 设置格式
            HSSFDataFormat dataFormat = this._workbook.createDataFormat();
            cellStyle.setDataFormat(dataFormat.getFormat(format));
            // 设置单元格样式
            cell.setCellStyle(cellStyle);
        }
    }

    private void setCellStyle(HSSFCell cell) {
        // 创建样式对象
        HSSFCellStyle cellStyle = _workbook.createCellStyle();
        // 创建字体对象
        HSSFFont font = _workbook.createFont();
        // 设置字体大小
        font.setFontHeightInPoints((short) 15);
        // 设置粗体
        font.setBold(false);
        // 设置为宋体
        font.setFontName("宋体");
        // 将字体加入到样式对象
        cellStyle.setFont(font);
        // 设置水平对齐方法
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直对齐方法
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 设置边框
        cellStyle.setBorderTop(BorderStyle.THIN);  // 顶部边框粗线
        cellStyle.setTopBorderColor((short) 0xFFFF00);  // 设置颜色
        // 设置单元格样式
        cell.setCellStyle(cellStyle);
    }

    public void write(OutputStream outputStream) throws WriterException, IOException {
        _workbook.write(outputStream);
    }

    public void write(File file) throws IOException {
        if (file == null) {
            throw new IllegalArgumentException("param of file can not be null");
        }
        if (file.isDirectory()) {
            throw new IllegalArgumentException("param of file must be a file object");
        }
        String fileName = file.getName();
        if ("".equals(fileName.trim())) {
            throw new IllegalArgumentException("file error");
        }
        // 写入到文件
        _workbook.write(file);
    }

    public void test() throws IOException {
        ExcelUtils excel = ExcelUtils.init();
        excel.createSheet("gzb");
        excel.createRow();
        for (int i = 0; i < 5; i++) {
            excel.addCell(i);
        }
        excel.addCell(new Date());
        excel.createRow();
        excel.createRow();
        excel.addCell(new Date(), "yyyy年MM月dd日");
        excel.write(new File("2003.xls"));
    }
}
